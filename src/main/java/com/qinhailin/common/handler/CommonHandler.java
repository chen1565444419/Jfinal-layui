/**
 * Copyright 2019-2024 覃海林(qinhaisenlin@163.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

package com.qinhailin.common.handler;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jfinal.aop.Aop;
import com.jfinal.handler.Handler;
import com.jfinal.kit.PropKit;
import com.qinhailin.common.kit.IpKit;
import com.qinhailin.common.model.SysUser;
import com.qinhailin.pub.login.service.LoginService;

/**
 * 全局路由处理
 * 
 * @author qinhailin
 *
 */
public class CommonHandler extends Handler {
	LoginService loginService=Aop.get(LoginService.class);
	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {		

		if (target.startsWith("/portal/download/")) {
			// 下载上传的文件
			int len="/portal/download/".length();
			request.setAttribute("url", target.substring(len));
			target = target.substring(0, len-1);
		} else if (target.startsWith("/portal/delete/")) {
			// 删除上传文件
			int len="/portal/delete/".length();
			request.setAttribute("url", target.substring(len));
			target = target.substring(0, len-1);
		} else if (target.startsWith("/portal/go/")) {
			// 公共/portal/go路由
			int len="/portal/go/".length();
			String view = target.substring(len);
			request.setAttribute("view", view);
			target = target.substring(0, len-1);
		} else if (target.startsWith("/portal/temp/")) {	
			// 下载模板文件
			int len="/portal/temp/".length();
			try {
				request.setAttribute("url", URLDecoder.decode(target.substring(len-1), "UTF-8"));
				System.out.println(URLDecoder.decode(target.substring(len-1), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			target = target.substring(0, len-1);
		}else if(target.startsWith("/portal/view/")){
			// 图片预览
			int len="/portal/view/".length();
			request.setAttribute("url", target.substring(len));
			target = target.substring(0, len-1);
		}else if(target.startsWith("/ureport")){
			if(!PropKit.getBoolean("startUreport2")){
				target="/portal/go";
				request.setAttribute("view", "common/error/400");
				request.setAttribute("msg", "报表功能已关闭，请联系管理员！");
			}
			Cookie[] cookies = request.getCookies();
			String sessionId=null;
			if (cookies != null)
				for (Cookie cookie : cookies)
					if (cookie.getName().equals(LoginService.sessionIdName))
						sessionId=cookie.getValue();
			if(sessionId==null){
				HttpSession session = request.getSession(false);
				sessionId=(String) session.getAttribute("sessionId");
			}
			if (sessionId != null) {
				SysUser loginAccount = null;
				loginAccount = loginService.getLoginAccountWithSessionId(sessionId);
				if (loginAccount == null) {
					String loginIp = IpKit.getRealIp(request);
					loginAccount = loginService.loginWithSessionId(sessionId, loginIp);
				}
				if (loginAccount == null) {
					target="/pub/login";
				}
			}else{
				target="/pub/login";
			}
		}
		
		next.handle(target, request, response, isHandled);
	}

}
